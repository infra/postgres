FROM postgres:13-alpine

RUN apk add --no-cache --virtual .cjk-build-deps \
    gcc icu-dev git make libc-dev llvm15-dev clang15 g++ && \
    mkdir -p /root && \
    cd /root && \
    git clone https://github.com/huangjimmy/pg_cjk_parser.git parser && \
    cd /root/parser && \
    make clean && make install && \
    apk del --no-network .cjk-build-deps
